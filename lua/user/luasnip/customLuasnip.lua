local ls = require"luasnip"
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")

-- i(1, "cond"), t(" ? "), i(2, "then"), t(" : "), i(3, "else")
ls.snippets = {
  lua = {
    s("t", {
      t({"t({'"}),i(0), t({"'}), "}),
    }),
    s("tl", {
      t({"t({'"}),i(0), t({"',''}), ",}),
    }),
    s("i", {
      t({"i("}), i(1,"num"), t({', "'}), i(2), t({'"), '})
    }),
    s("s", {
        t({'s("'}), i(1, "trigger"), t({'", {' ,''}),
        t({'  '}), i(2),
        t({'','}),'}),
    }),
  },
  go = {
    s("snirou", {
      -- i(1, "e"), t({'.GET("/'}), i(2, "endpoint"), t({'", '}), i(3, "handler"), t({'.'}), i(4, "Function"), t({')'}),        
      i(1, "g"), t({'.'}), i(2, "method"), t({'("/'}),  i(3, "endpoint"), t({'", '}), i(4, "handler"), t({'.'}), i(5, "Function"), t({')'}),       
    }),
    s("snipost", {
      -- t({'func ('}), i(1, "g"), t({' *'}), i(2, "Handler"), t({') '}), i(3, "FunctionName"), t({'(ctx echo.Context) error {',''}),      
      t({'type Req struct {',''}), 
      t({'  '}), i(4, "Variable"), t({' '}), i(5, "type"), t({' `json:"'}), i(6, "variable"), t({'" validate:"'}), i(7, "required"), t({'"`',''}),        
      t({'}',''}), 
      t({'req := Req{}',''}), 
      t({'defer ctx.Request().Body.Close()',''}), 
      t({'','if err := json.NewDecoder(ctx.Request().Body).Decode(&req); err != nil {',''}), 
      t({'  httpError := utils.BadRequest(err.Error())',''}), 
      t({'  return ctx.JSON(httpError.Status, httpError)',''}), 
      t({'}',''}), 
      t({''}),
      -- t({'','}',''}), 
    }),
    s("sniapi", {
      t({'func ('}), i(1, "g"), t({' *'}), i(2, "Handler"), t({') '}), i(3, "FunctionName"), t({'(ctx echo.Context) error {',''}),
      t({'  '}), i(0),
      t({'','}',''}), 
    }),
    s("snival", {
      t({'if err := utils.Validate('}), i(1, "req"), t({'); err != nil{',''}),   
      t({'  httpError := utils.BadRequest(err.Error())',''}), 
      t({'  return ctx.JSON(httpError.Status, httpError)',''}), 
      t({'}'},''), 
    }),
    s("snierr", {
      t({'if err != nil {', ''}), 
      t({'  '}), i(1, "err handling"),  
      t({'','}'}), 
    }),
    s("snih400", {
      t({'httpError := utils.BadRequest('}), i(1,"err.Error()"), t({')',''}),
      t({'return ctx.JSON(httpError.Status, httpError)'}), 
    }),
    s("snih500", {
      t({'httpError := utils.InternalServerError('}), i(1,"err.Error()"),t({', "'}),i(2, "errorCode"), t({'")',''}),
      t({'return ctx.JSON(httpError.Status, httpError)'}), 
    }),
    s("snires", {
      t({'res := struct {',''}), 
      t({'  utils.Response',''}), 
      t({'  '}), i(1, "Key"), t({' '}), i(2, "type"), t({' `json:"'}), i(3, "key"), t({'"`', ''}),   
      t({'}{', ''}), 
      t({'  utils.Success(),',''}), 
      t({'  '}), i(4, "value"), t({',',''}), 
      t({'}',''}), 
      t({'return ctx.JSON(res.Status, res)'}), 
    }),
    s("snimodel", {
      t({'type '}), i(1, "Name"), t({' struct {',''}),  
      t({'  mgm.DefaultModel `bson:",incline"`',''}), 
      t({'  '}), i(2, "Field"), t({' '}), i(3, "type"), t({' `json:"'}), i(4, "field"), t({'" bson:"'}), i(0, "field"), t({'"`',''}), 
      t({'}',''}), 
    }),
    s("snicatch", {
      t({'defer func() {',''}), 
      t({'  if err := recover(); err != nil {',''}), 
      t({'    Logger.Error().Msgf("Error: ", err)',''}), 
      t({'  }',''}), 
      t({'}()'}), 
    }),
    s("sniselect", {
      t({'for i := 0; i < '}), i(1, "chanNum"), t({' ; i++ {',''}),  
      t({'  select {',''}), 
      t({'  case '}), i(0, "case"), t({':',''}),   
      t({'  }',''}), 
      t({'}',''}), 
    }),
    s("sniwg", {
      t({'var wg sync.WaitGroup',''}), 
      t({'wg.Add('}),i(1, "num"),t({')',''}),   
    }),
    s("snieg", {
      t({'eg := errgroup.Group{}',''}), 
      t({'eg.Go(func() error {',''}), 
      i(0, ""), 
      t({'  return err',''}), 
      t({'})',''}), 
    }),
    s("snilogobj", {
      t({'Logger.Debug().Msgf("%+v\\n", ',}), i(1, "object"), t({')',''}),  
    }),
  },
}
